package com.blumeglobal.eventmanager;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class AdapterService {
	
	private static final Logger LOGGER = LogManager.getLogger(AdapterService.class);	
	@RequestMapping("/startAdapter")
	public String startAdapter() {
		LOGGER.info("This is from Controller Class info");
		LOGGER.debug("This is from Controller Class debug");
		LOGGER.error("This is from Controller Class error");
		return "Started cron tasks";
	}
}
