/*
    ./client/index.js
    which is the webpack entry file
*/

import React from 'react';
import ReactDOM from 'react-dom';
import Readux from "react-redux";

import TabManager from './components/TabManager.jsx';

ReactDOM.render(<TabManager />, document.getElementById('root'));